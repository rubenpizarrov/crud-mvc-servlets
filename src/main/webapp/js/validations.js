const form = document.getElementById('form-validate');
const btnRegistrar = document.getElementById('btnRegistrar');
const nombreInput = document.getElementById('txtNombre');
const apellidoInput = document.getElementById('txtApellido');
const correoInput = document.getElementById('txtCorreo');
const telefonoInput = document.getElementById('txtTelefono');
const password1Input = document.getElementById('txtPassword1');
const password2Input = document.getElementById('txtPassword2');


form.addEventListener('submit', function(e) {
    if (!enviarDatos()) {
        e.preventDefault();
        alert('Hay campos sin llenar revise el formulario o errores');
    } else {
        
    
    }
});

btnRegistrar.addEventListener('click', enviarDatos);

function enviarDatos() {


    let nombreValue = nombreInput.value.trim();
    let apellidoValue = apellidoInput.value.trim();
    let telefonoValue = telefonoInput.value.trim();
    let correoValue = correoInput.value.trim();
    let password1Value = password1Input.value.trim();
    let password2Value = password2Input.value.trim();


    if (validaNombre(nombreValue) &&
            validaApellido(apellidoValue) &&
            validaTelefono(telefonoValue) &&
            validaCorreo(correoValue) &&
            validaPassword(password1Value, password2Value))
    {
        return true;
    } else {
        return false;
    }
    
    return false;


}

function validaNombre(input) {
    if (input === "" || input.length > 50 || !isLetters(input)) {
        nombreInput.classList.add("is-invalid");
        nombreInput.classList.remove("is-valid");
        return false;
    } else {
        nombreInput.classList.remove("is-invalid");
        nombreInput.classList.add("is-valid");
        return true;
    }
}



function validaApellido(input) {

    if (input === "" || input.length > 50 || !isLetters(input)) {
        apellidoInput.classList.add("is-invalid");
        apellidoInput.classList.remove("is-valid");
        return false;
    } else {
        apellidoInput.classList.remove("is-invalid");
        apellidoInput.classList.add("is-valid");
        return true;
    }
}

function validaCorreo(input) {
    if (!emailIsValid(input) || input === "") {
        correoInput.classList.add('is-invalid');
        correoInput.classList.remove('is-valid');
        return false;
    } else {
        correoInput.classList.remove('is-invalid');
        correoInput.classList.add('is-valid');
        return true;
    }
}

function validaTelefono(input) {
    if (!isaNumber(input) || input.length > 8) {
        telefonoInput.classList.add('is-invalid');
        telefonoInput.classList.remove('is-valid');
        return false;
    } else {
        telefonoInput.classList.remove('is-invalid');
        telefonoInput.classList.add('is-valid');
        return true;
    }
}

function validaPassword(input1, input2) {
    if (input1 !== input2 || input1.length < 8) {
        password1Input.classList.add('is-invalid');
        password1Input.classList.remove('is-valid');
        password2Input.classList.add('is-invalid');
        password2Input.classList.remove('is-valid');
        return false;
    } else {
        password1Input.classList.remove('is-valid');
        password1Input.classList.add('is-invalid');
        password2Input.classList.remove('is-valid');
        password2Input.classList.add('is-invalid');
        return true;
    }
}





function isaNumber(numero) {
    return /^[0-9]+$/.test(numero);
}

function isLetters(letras) {
    return /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/.test(letras);
}

function isLettersSpace(letras) {
    return /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/.test(letras);
}


function isLettersSpacesNumbers(letras) {
    return /^[a-z0-9A-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-z0-9A-ZÀ-ÿ\u00f1\u00d1]*)*[a-z0-9A-ZÀ-ÿ\u00f1\u00d1]+$/.test(letras);
}

////Expresion Regular comprueba solo @ y . 
function emailIsValid(email) {
    return /\S+@\S+\.\S+/.test(email);
}