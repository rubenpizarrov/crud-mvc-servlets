<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
    
    
    <section id="about">
        <div class="container">
            <div class="row justify-content-center section-title">
                <h2 class="section-title-heading">Nuestro Servicios</h2>
            </div>

            <div class="row justify-content-center text-center mt-5">
                <div class="col-md-4">
                    <i class="fas fa-biking fa-3x mb-2"></i>
                    <h3>Pedidos a Domicilio</h3>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maxime quibusdam nesciunt voluptatum
                        fugit possimus autem minus similique suscipit nemo dignissimos. Dolorum eius quas rem
                        praesentium alias fugit expedita provident voluptas.</p>
                </div>
                <div class="col-md-4">
                    <i class="fas fa-people-carry fa-3x mb-2"></i>
                    <h3>VAridad de Productos</h3>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maxime quibusdam nesciunt voluptatum
                        fugit possimus autem minus similique suscipit nemo dignissimos. Dolorum eius quas rem
                        praesentium alias fugit expedita provident voluptas.</p>
                </div>
                <div class="col-md-4">
                    <i class="fas fa-comment-dollar fa-3x mb-2"></i>
                    <h3>Los Mejores Precios</h3>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maxime quibusdam nesciunt voluptatum
                        fugit possimus autem minus similique suscipit nemo dignissimos. Dolorum eius quas rem
                        praesentium alias fugit expedita provident voluptas.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="ordenes">
        <div class="container mb-5">
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </section>
    
    <jsp:include page="footer.jsp"></jsp:include>
    
    </body>
</html>