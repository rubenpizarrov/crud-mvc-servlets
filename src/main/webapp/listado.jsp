<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>
    <jsp:include page="header.jsp"></jsp:include>
        <section id="blanca">
            <div class="container">
                <div class="row justify-content-center section-title mb-5">
                    <h2 class="section-title-heading">Administracion</h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <h2>Mantenedor Usuarios</h2>
                        <div class="alert alert-light" role="alert">

                        </div>
                    </div>
                    <div class="col-md-12">
                        <a class="btn btn-primary" href="producto_create.jsp" role="button">Crear Nuevo</a>
                        <a class="btn btn-primary" href="index.jsp" role="button">Volver al Crud</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 mt-3">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Codigo</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Marca</th>
                                    <th scope="col">Imagen</th>
                                    <th scope="col">Stock</th>
                                    <th scope="col">Precio</th>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${lstProduct}" var="aux">
                                <tr>
                                    <td><c:out value="${aux.getCodigo()}"/></td>
                                    <td><c:out value="${aux.getNombre()}"/></td>
                                    <td><c:out value="${aux.getMarca()}"/></td>
                                    <td><img src="<c:out value="${aux.getRuta()}"/>" width="100" height="100"></td>
                                    <td><c:out value="${aux.getStock()}"/></td>
                                    <td><c:out value="${aux.getPrecio()}"/></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </section>



    <jsp:include page="footer.jsp"></jsp:include>

</body>

</html>