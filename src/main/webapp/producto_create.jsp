<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="header.jsp"></jsp:include>
<section id="blanca">
	<div class="container">
		<div class="row justify-content-center section-title">
			<h2 class="section-title-heading">Administracion</h2>
		</div>

		<!--  this.codigo = codigo;
		this.descripcion = descripcion;
		this.marca = marca;
		this.categoria = categoria;
		this.costo = costo;
		this.venta = venta;
		this.stock = stock; -->

		<div class="row justify-content-center">

			<div class="col-md-8 col-sm-12">
				<form action="ProductoController" method="post">
					<h2>Mantenedor Productos</h2>
					<div class="alert alert-danger" role="alert">${msg}</div>

					<div class="form-group">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<label for="txtCodigo">Codigo</label> <input type="text"
									class="form-control" id="txtCodigo" name="txtCodigo"
									placeholder="1111" required>
								<div class="invalid-feedback">Solo se permiten letras en
									este campo</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<label for="txtNombre">Nombre</label> <input type="text"
									class="form-control" id="txtNombre" name="txtNombre"
									placeholder="Nombre" required>
								<div class="invalid-feedback">Solo se permiten letras en
									este campo</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<label for="txtMarca">Marca</label> 
								<input type="text"
									class="form-control" id="txtMarca" name="txtMarca"
									placeholder="Marca" required>
								<div class="invalid-feedback">Solo se permiten letras en
									este campo</div>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<label for="txtRuta">Ruta Imagen</label> <input
									type="text" class="form-control" id="txtRuta"
									name="txtRuta" required>
								<div class="invalid-feedback">Solo se permiten letras en
									este campo</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<label for="txtStock">Stock</label> <input type="text"
									class="form-control" id="txtStock" name="txtStock"
									required>
								<div class="invalid-feedback">Solo se permiten letras en
									este campo</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<label for="txtPrecio">Precio</label> <input
									type="text" class="form-control" id="txtPrecio"
									name="txtPrecio" required>
								<div class="invalid-feedback">Solo se permiten letras en
									este campo</div>
							</div>
						</div>
					</div>
					
					



					<div class="d-flex justify-content-end">
						<button type="reset" class="btn btn-secondary" name="btnLimpiar">Limpiar</button>
						<button type="submit" class="btn btn-primary" name="btnAction"
							value="registrar">Registrar</button>
					</div>

				</form>
			</div>

		</div>
	</div>

</section>



<jsp:include page="footer.jsp"></jsp:include>

</body>

</html>
