package Model;

public class Producto {
	
	private int codigo;
    private String nombre;
    private String marca;
    private String ruta;
    private int stock;
    private int precio;
    
    
	public Producto(int codigo, String nombre, String marca, String ruta, int stock, int precio) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.marca = marca;
		this.ruta = ruta;
		this.stock = stock;
		this.precio = precio;
	}


	public int getCodigo() {
		return codigo;
	}


	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public String getRuta() {
		return ruta;
	}


	public void setRuta(String ruta) {
		this.ruta = ruta;
	}


	public int getStock() {
		return stock;
	}


	public void setStock(int stock) {
		this.stock = stock;
	}


	public int getPrecio() {
		return precio;
	}


	public void setPrecio(int precio) {
		this.precio = precio;
	}


	@Override
	public String toString() {
		return "Producto [codigo=" + codigo + ", nombre=" + nombre + ", marca=" + marca + ", ruta=" + ruta + ", stock="
				+ stock + ", precio=" + precio + "]";
	}
	
	
	
    

    
    
	
    
    
    
}
