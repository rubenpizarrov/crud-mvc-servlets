package DAL;

import java.util.ArrayList;

import Model.Producto;

public class ProductoColeccion {
	
	ArrayList<Producto> listaProductos = new ArrayList<>();
	
	public void agregar(Producto p) {
		listaProductos.add(p);
	}
	
	public ArrayList<Producto> ObtenerTodos() {
		return listaProductos;
	}
}
