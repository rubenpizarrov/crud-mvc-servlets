package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAL.ProductoColeccion;
import Model.Producto;

/**
 * Servlet implementation class ProductoController
 */
public class ProductoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ProductoColeccion productos = new ProductoColeccion();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductoController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		int codigo = Integer.parseInt(request.getParameter("txtCodigo"));
		String nombre = request.getParameter("txtNombre");
		String marca = request.getParameter("txtMarca");
		String ruta = request.getParameter("txtRuta");
		int stock = Integer.parseInt(request.getParameter("txtStock"));
		int precio = Integer.parseInt(request.getParameter("txtPrecio"));
		
		Producto p = new Producto(codigo, nombre, marca, ruta, stock, precio);
		
		productos.agregar(p);
		
		request.getSession().setAttribute("msg", "Se ingresó correctamente");
		request.getSession().setAttribute("lstProduct", productos.ObtenerTodos());
		response.sendRedirect("listado.jsp");
		
		
	}

}
